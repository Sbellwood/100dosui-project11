//
//  Genre.swift
//  Bookworm
//
//  Created by Skyler Bellwood on 7/30/20.
//

import Foundation

enum Genre: CaseIterable, Identifiable {
    var id: Genre { self }
    var name: String {
        return "\(self)"
    }
    static var genresWithAssets = ["Fantasy", "Horror", "Kids", "Mystery", "Poetry", "Romance", "Thriller"]
    
    case Fantasy, Horror, Kids, Mystery, Poetry, Romance, Thriller, Nonfiction, Classic
}
