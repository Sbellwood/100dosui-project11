//
//  File.swift
//  Bookworm
//
//  Created by Skyler Bellwood on 7/30/20.
//

import Foundation

extension Book {
    
    var formattedDate: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        
        if let date = self.dateFinished {
            return dateFormatter.string(from: date)
        } else {
            return "Unknown date"
        }
    }
}
